function warning(msg) {
    return `
    <div class="alert alert-warning" role="alert">
    Sorry nothing is showing! Try again.
    </div>`
  }

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';       
    
    try {
        const response = await fetch(url);
                
        if (!response.ok)   {
            throw new Error('Response not ok')
        } else {
            const data = await response.json();

            const selectTag = document.getElementById('conference');
            for (let conference of data.conferences) {                
                const option = document.createElement('option');                           
                option.value = conference.href;
                option.innerHTML = conference.name;
                selectTag.appendChild(option);
                console.log(conference)
            }
            const formTag = document.getElementById('create-presentation-form');
            formTag.addEventListener('submit', async event => {
                event.preventDefault();                
                const formData = new FormData(formTag);
                const json = JSON.stringify(Object.fromEntries(formData));
                const conferencehref = Object.fromEntries(formData)["conference"]
                const conferenceUrl = `http://localhost:8000${conferencehref}presentations/`;
                const fetchConfig = {
                    method: "POST",
                    body: json,
                    headers: {
                    'Content-Type': 'application/json',
                    },
                };
                const response = await fetch(conferenceUrl, fetchConfig);
                if (response.ok) {
                    formTag.reset();                
                    const newConference = await response.json();                
                };
            })
        }   
    } catch (e) {
        console.error('error', e);
        const alert = warning(e.msg)
        const row = document.querySelector(".form-control")
        row.innerHTML = alert + row.innerHTML;
        }    
    
});
